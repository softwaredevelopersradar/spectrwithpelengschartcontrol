package KvetkaSpectrWithPelengsLib.Exceptions;

public class NotFoundHeadException extends Exception{

    private double startFrequency;
    private double endFrequency;

    public double getStartFrequency()
    {
        return startFrequency;
    }
    public double getEndFrequency()
    {
        return endFrequency;
    }

    public NotFoundHeadException(String message, double startFrequency, double endFrequency)
    {
        super(message);
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
    }

}
