package KvetkaSpectrWithPelengsLib.Drawing;

import de.gsi.chart.XYChart;
import de.gsi.chart.plugins.ChartPlugin;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.lang.reflect.Array;
import java.util.Arrays;

public class RangeHead extends ChartPlugin {


    private int index;
    private double startFrequency;
    private double endFrequency;
    private final double upLevelBound = -60;
    private final Line leftYLine = new Line();
    private final Line rightYLine = new Line();
    private final Line XLine = new Line();

    public RangeHead(XYChart chart, double startFrequency, double endFrequency)
    {
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
        chartProperty().set(chart);
        layoutChildren();
    }

    public RangeHead(XYChart chart, double startFrequency, double endFrequency, int index)
    {
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
        this.index = index;
        chartProperty().set(chart);
        layoutChildren();
    }



    @Override
    public void layoutChildren()
    {
        double startSceneX = getChart().getAxes().get(0).getDisplayPosition(startFrequency);
        double endSceneY = getChart().getAxes().get(0).getDisplayPosition(endFrequency);

        double downSceneY = getChart().getAxes().get(1).getDisplayPosition(-120);
        double upSceneY = getChart().getAxes().get(1).getDisplayPosition(upLevelBound);

        layoutLine(leftYLine,startSceneX,downSceneY,startSceneX,upSceneY);
        layoutLine(rightYLine,endSceneY,downSceneY,endSceneY,upSceneY);
        layoutLine(XLine,startSceneX,upSceneY,endSceneY,upSceneY);


        addChildNodeIfNotPresent(leftYLine);
        addChildNodeIfNotPresent(rightYLine);
        addChildNodeIfNotPresent(XLine);

        for(int i = 0; i < getChartChildren().size(); i++) {
            if(getChartChildren().get(i).getClass() == Stick.class)
            {
                ((Stick)getChartChildren().get(i)).UpdateLayout();
            }
        }

    }

    protected void layoutLine(Line line,final double startX, final double startY, final double endX, final double endY) {
        line.setStartX(startX);
        line.setStartY(startY);
        line.setEndX(endX);
        line.setEndY(endY);
        line.setStroke(Color.WHITE);
        line.setStrokeWidth(2);

        addChildNodeIfNotPresent(line);
    }

    public double getStartFrequency() {
        return startFrequency;
    }

    public double getEndFrequency() {
        return endFrequency;
    }

    public double getIndex() {return index;}

    public void setStartFrequency(double startFrequency) {this.startFrequency = startFrequency;}

    public void setEndFrequency(double endFrequency) {this.endFrequency = endFrequency;}



    public void addStick(double frequency)
    {
        if(frequency < startFrequency || frequency > endFrequency)
            return;
        Stick stick = new Stick(getChart().getAxes(), frequency, upLevelBound);
        addChildNodeIfNotPresent(stick);
    }

    public void setSticks(double[] frequencies)
    {
        clearAllSticks();
        for(int i = 0; i < frequencies.length; i++)
        {
            addChildNodeIfNotPresent(new Stick(getChart().getAxes(),frequencies[i], upLevelBound));
        }
    }

    public void clearAllSticks()
    {
        ObservableList observableList = getChartChildren();
        for(int i = 0 ; i<observableList.size(); i++)
        {
            if(observableList.get(i).getClass() == Stick.class)
            {
                observableList.remove(i);
                i--;
            }
        }
    }

    public void removeStick(int index)
    {
        if(getChartChildren().get(index + 2).getClass() == Stick.class)
             getChartChildren().remove(index + 2);
    }

    public void removeStick(double frequency)
    {
        ObservableList observableList = getChartChildren();
        for(Object stick : observableList)
        {
            if(stick.getClass() == Stick.class)
            {
                if(Math.abs(((Stick) stick).getFrequency() - frequency) <= 0.5)
                {
                    getChartChildren().remove(stick);
                    return;
                }
            }
        }
    }

    protected void addChildNodeIfNotPresent(final Node node) {
        if (!getChartChildren().contains(node)) {
            getChartChildren().add(node); // add elements always at the bottom so they cannot steal focus
        }
    }


}
