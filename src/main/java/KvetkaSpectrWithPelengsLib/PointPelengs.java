package KvetkaSpectrWithPelengsLib;

import KvetkaSpectrWithPelengsLib.CustomDataPointToolTip.CustomDataToolTip;
import KvetkaSpectrWithPelengsLib.CustomIndicators.XValueIndicatorCustom;
import KvetkaSpectrWithPelengsLib.CustomRenders.LabelledMarkerRenderCustom;
import KvetkaSpectrWithPelengsLib.CustomStringConverters.CustomTickLabelFormatter;
import KvetkaSpectrWithPelengsLib.CustomZoomer.CustomZoomer;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.AxisMode;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.renderer.ErrorStyle;
import de.gsi.chart.renderer.LineStyle;
import de.gsi.chart.renderer.spi.ErrorDataSetRenderer;
import de.gsi.chart.ui.geometry.Side;
import de.gsi.dataset.spi.DoubleDataSet;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class PointPelengs<T> extends XYChart {

    private static final int BUFFER_CAPACITY = 11000;
    private double maximumXValue = 30000;
    private double minimumXValue = 1500;
    private final double maximumYValue = 360;
    private final double minimumYValue = 0;
    private final double indicatorXDefaultValue = 10000;
    private int amountDisplayedOldPelengs = 3;
    private int indexDisplayedOldPelengs = 0;

    private final String axisXName = "Частота";
    private final String axisXUnit = "кГц";
    private final String axisYName = "Угол";
    private final String axisYUnit = " ° ";
    private final String indicatorXName = "Частота";

    private final CustomZoomer zoom = new CustomZoomer();
    private final List<Integer> arrayLenghtsList = new ArrayList<>();
    private final CustomDataToolTip customDataToolTip = new CustomDataToolTip();
    private final DoubleDataSet dataBuffer = new DoubleDataSet("pelengs", BUFFER_CAPACITY);
    private final ErrorDataSetRenderer dipoleCurrentRenderer = new ErrorDataSetRenderer();
    private final LabelledMarkerRenderCustom labelledMarkerRendered = new LabelledMarkerRenderCustom();

    private XValueIndicatorCustom xValueIndicatorCustom;

    public PointPelengs()
    {
        super(new DefaultNumericAxis(),new DefaultNumericAxis());
        initComponents();
    }

    public double getMinimumXValue() {return  this.minimumXValue; }

    public double getMaximumXValue()
    {
        return this.maximumXValue;
    }



    public void setMaximumXValue(double maximumXValue)
    {
        this.maximumXValue = maximumXValue;
    }

    public void setMinimumXValue(double minimumXValue) {this.minimumXValue = minimumXValue; }

    public void setValueXIndicatorCustom(double value)
    {
        xValueIndicatorCustom.setValue(value);
    }

    public void setAmountDisplayedOldPelengs(int amountDisplayedOldPelengs) { this.amountDisplayedOldPelengs = amountDisplayedOldPelengs;}

    public void setStartXAxis(double x1)
    {
        if(x1 != getMinimumXValue())
        {
            this.getXAxis().setMin(x1);
            setMinimumXValue(x1);
        }
    }

    public void setEndXAxis(double x2)
    {
        if(x2 != getMaximumXValue())
        {
            this.getXAxis().setMax(x2);
            setMaximumXValue(x2);
        }
    }



    public double[] PlotPelengs(double[] angle,  double[] freq)
    {
        ManageOldPelengs(angle);

        dataBuffer.add(freq,angle);

        AddDataLabel(freq.length);

        arrayLenghtsList.add(freq.length);

        return freq;
    }

    public double[] PlotPelengs(double[] angle, double stepFrequency, double startFrequency)
    {
        ManageOldPelengs(angle);

        double[] xx_d = PlotOneSetOfPelengs(angle, stepFrequency,startFrequency);

        arrayLenghtsList.add(xx_d.length);
        return xx_d;
    }


    private void initComponents()
    {
        this.setAnimated(false);
        this.setLegendVisible(false);

        this.getXAxis().setName(axisXName); // init X axis
        this.getXAxis().setUnit(axisXUnit);
        this.getXAxis().set(minimumXValue, maximumXValue);
        this.getXAxis().setAutoRanging(false);
        ((DefaultNumericAxis)this.getXAxis()).setTickLabelFormatter(new CustomTickLabelFormatter());

        this.getYAxis().setName(axisYName); // init Y axis
        this.getYAxis().setUnit(axisYUnit);
        this.getYAxis().setAutoRanging(false);
        this.getYAxis().setSide(Side.LEFT);
        this.getYAxis().set(minimumYValue, maximumYValue);

        xValueIndicatorCustom = new XValueIndicatorCustom(this.getXAxis(), indicatorXDefaultValue, indicatorXName);

        InitZoomer();
        initLabelledDataSetRender(labelledMarkerRendered);
        initErrorDataSetRenderer(dipoleCurrentRenderer);

        this.getPlugins().add(zoom);
        this.getPlugins().add(customDataToolTip);
        this.getPlugins().add(xValueIndicatorCustom);

        dataBuffer.setStyle("strokeColor=" + Color.rgb(255,201,0,1)+";" +
                "markerType=circle;"+
                "markerSize=5;" +
                "fillColor=" + Color.rgb(108,146,254,1)+";");
    }

    private void InitZoomer()
    {
        zoom.setSliderVisible(false);
        zoom.setAxisMode(AxisMode.X);
        zoom.setAddButtonsToToolBar(false);
        zoom.setZoomInMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED && mouseEvent.getClickCount() == 1);
        zoom.setZoomOriginMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getClickCount() == 2);
        zoom.setZoomOutMouseFilter(mouseEvent -> false);
    }

    private void initErrorDataSetRenderer(final ErrorDataSetRenderer eRenderer) {
        eRenderer.setPolyLineStyle(LineStyle.NONE);
        eRenderer.setErrorType(ErrorStyle.NONE);
        eRenderer.setDrawMarker(true);
        eRenderer.getAxes().addAll(this.getYAxis());
        this.getRenderers().add(eRenderer);
        eRenderer.getDatasets().add(dataBuffer);
    }

    private void initLabelledDataSetRender(final LabelledMarkerRenderCustom eRenderer)
    {
        eRenderer.getAxes().addAll(this.getYAxis());
        eRenderer.getDatasets().add(dataBuffer);
        eRenderer.enableVerticalMarker(true);
        this.getRenderers().add(eRenderer);
    }

    private double[] PlotOneSetOfPelengs(double[] angle, double stepFrequency, double startFrequency)
    {
        ArrayList<Double> xx = new ArrayList<>();
        ArrayList<Double> yy = new ArrayList<>();
        double freq = startFrequency;

        for (double v : angle) {
            if (v - (-10) > 0.0001) {
                xx.add(freq);
                yy.add(v);
            }
            freq += stepFrequency;
        }


        double[] xx_d = xx.stream().mapToDouble(Double::doubleValue).toArray();
        double[] yy_d = yy.stream().mapToDouble(Double::doubleValue).toArray();

        dataBuffer.add(xx_d,yy_d);

        AddDataLabel(xx_d.length);
        return xx_d;
    }

    private void ManageOldPelengs(double[] angle)
    {
        int currentIndex = 0;
        if(indexDisplayedOldPelengs == amountDisplayedOldPelengs)
        {
            if(arrayLenghtsList.size() > 0)
            {
                for(int i = 0; i < arrayLenghtsList.get(0); i++)
                    dataBuffer.remove(0);
                arrayLenghtsList.remove(0);
                indexDisplayedOldPelengs--;
            }
        }

        for(int i = 0; i < arrayLenghtsList.size(); i++)
        {
            for(int j = currentIndex; j < currentIndex + arrayLenghtsList.get(i);j++)
            {
                int colorDiff = 0;
                if(i + 1  < arrayLenghtsList.size())
                    colorDiff = (int) (100/amountDisplayedOldPelengs * ((double) arrayLenghtsList.size()/(i+1))) + 15;

                dataBuffer.addDataStyle(j, "strokeColor=" + Color.rgb(255-colorDiff,205-colorDiff,0,(double) (i+1)/arrayLenghtsList.size()) + ";");
            }
            currentIndex += arrayLenghtsList.get(i);
        }

        indexDisplayedOldPelengs++;
    }

    private void AddDataLabel(int lastArrayLength)
    {
        int length = dataBuffer.getDataCount();
        for(int i = 0; i < length; i++)
        {
            dataBuffer.removeDataLabel(i);
            if(i >= length - lastArrayLength)
            {
                dataBuffer.addDataLabel(i, " ");
            }
        }
    }

}