package KvetkaSpectrWithPelengsLib.CustomIndicators;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.XRangeIndicator;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;

public class XRangeIndicatorCustom extends XRangeIndicator {

  private double width;
   public XRangeIndicatorCustom(Axis axis, double lowerBound, double upperBound) {
       super(axis, lowerBound,upperBound);
       width = upperBound;
   }

   private Color cl = Color.BLUE;
   public void setColor(Color cl)
   {
       this.cl = cl;
   }

    @Override
    public void layoutChildren() {
        if (getChart() == null) {
            return;
        }
        final Bounds plotAreaBounds = getChart().getCanvas().getBoundsInLocal();
        final double minX = plotAreaBounds.getMinX();
        final double maxX = plotAreaBounds.getMaxX();
        final double minY = plotAreaBounds.getMinY();
        final double maxY = plotAreaBounds.getMaxY();

        final Axis xAxis = getAxis();
        final double value1 = xAxis.getDisplayPosition(getLowerBound());
        final double value2 = xAxis.getDisplayPosition(getUpperBound());

        final double startX = Math.max(minX, minX + Math.min(value1, value2));
        final double endX = Math.min(maxX, minX + Math.max(value1, value2));

        rectangle.setStroke(cl);
        layout(new BoundingBox(startX, minY, endX - startX, maxY - minY));
    }
}
