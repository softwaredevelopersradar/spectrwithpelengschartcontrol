package KvetkaSpectrWithPelengsLib.CustomIndicators;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.AbstractSingleValueIndicator;
import de.gsi.chart.plugins.XValueIndicator;
import de.gsi.chart.ui.geometry.Side;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class XValueIndicatorCustom  extends XValueIndicator {
    public XValueIndicatorCustom(Axis axis, double value, String text) {
        super(axis, value, text);
        this.setEditable(true);
        pickLine.setOnMousePressed(null);
        triangle.setOnMousePressed(null);
        pickLine.setOnMouseReleased(null);
        pickLine.setOnMouseEntered(null);
        triangle.setOnMouseReleased(null);
        triangle.setOnMouseEntered(null);
        label.setOnMouseReleased(null);
        label.setOnMouseEntered(null);
    }

    @Override
    protected void handleDragMouseEvent(final MouseEvent mouseEvent) {
        pickLine.setCursor(Cursor.DEFAULT);
        mouseEvent.consume();
    }

    private Color cl = Color.WHITE;
    public void setColor(Color cl)
    {
        this.cl = cl;
    }

    @Override
    public void layoutChildren() {
        if (getChart() == null) {
            return;
        }
        final Bounds plotAreaBounds = getChart().getCanvas().getBoundsInLocal();
        final double minX = plotAreaBounds.getMinX();
        final double maxX = plotAreaBounds.getMaxX();
        final double minY = plotAreaBounds.getMinY();
        final double maxY = plotAreaBounds.getMaxY();
        final double xPos = minX + getAxis().getDisplayPosition(getValue());
        final double axisPos;
        if (getAxis().getSide().equals(Side.BOTTOM)) {
            triangle.getPoints().setAll(0.0, -8.0, -8.0, 0.0, 8.0, 0.0);
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxis().getCanvas().localToScene(0, 0)).getY() + 6;
        } else {
            triangle.getPoints().setAll(0.0, 0.0, -8.0, -8.0, 8.0, -8.0);
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxis().getCanvas().localToScene(0, getAxis().getHeight())).getY() - 6;
        }
        final double xPosGlobal = getChart().getPlotForeground().sceneToLocal(getChart().getCanvas().localToScene(xPos, 0)).getX();

        if (xPos < minX || xPos > maxX) {
            getChartChildren().clear();
        } else {
            layoutMarker(xPosGlobal, axisPos + 4, xPos, maxY);
            this.line.setStroke(cl);
            this.line.setStrokeWidth(2.5);
            this.line.setFill(cl);
            layoutLine(xPos, minY, xPos, maxY);

            layoutLabel(new BoundingBox(xPos - 27, (-1) * (maxY-40), 0, maxY - minY), MIDDLE_POSITION, getLabelPosition());
        }


    }
    @Override
    public void updateStyleClass() {
        setStyleClasses(label, "x-", AbstractSingleValueIndicator.STYLE_CLASS_LABEL);
        setStyleClasses(triangle, "x-", AbstractSingleValueIndicator.STYLE_CLASS_MARKER);
    }
}
