package KvetkaSpectrWithPelengsLib.CustomIndicators;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.plugins.ValueIndicator;
import de.gsi.chart.ui.HiddenSidesPane;
import de.gsi.chart.ui.geometry.Side;
import de.gsi.dataset.event.EventSource;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

 public class XYValueIndicatorCustom  extends AbstractDoubleValueIndicatorCustom implements EventSource, ValueIndicator {

    public XYValueIndicatorCustom(final Axis axisX, final Axis axisY, final double valueX, final  double valueY, HiddenSidesPane pane) {
        this(axisX,axisY, valueX, valueY,pane,null,null);
    }

    public XYValueIndicatorCustom(final Axis axisX, final Axis axisY, final double valueX, final  double valueY, HiddenSidesPane pane, final String textX, final String textY) {
        super(axisX, axisY, valueX, valueY, textX, textY);
        setLabelHorizontalAnchor(HPos.RIGHT);
        setLabelPositionX(0.975);
        setLabelPositionY(0.975);
        pane.setOnMousePressed(this::handleClickMouseEvent);

    }
    protected void handleClickMouseEvent(final MouseEvent mouseEvent) {
       if(mouseEvent.getButton() == MouseButton.PRIMARY) {
           Point2D c = getChart().getPlotArea().sceneToLocal(mouseEvent.getSceneX(), mouseEvent.getSceneY());
           final double yPosData = getAxisY().getValueForDisplay(c.getY());
           final double xPosData = getAxisX().getValueForDisplay(c.getX());
           valuePropertyX().set(xPosData);
           valuePropertyY().set(yPosData);
       }
        mouseEvent.consume();
    }

    @Override
    public void layoutChildren() {
        if (getChart() == null) {
            return;
        }
        final Bounds plotAreaBounds = getChart().getCanvas().getBoundsInLocal();
        final double minX = plotAreaBounds.getMinX();
        final double maxX = plotAreaBounds.getMaxX();
        final double minY = plotAreaBounds.getMinY();
        final double maxY = plotAreaBounds.getMaxY();

        final double yPos = minY + getAxisY().getDisplayPosition(getValueY());
        final double axisPosY;
        if (getAxisY().getSide().equals(Side.RIGHT)) {
            axisPosY = getChart().getPlotForeground().sceneToLocal(getAxisY().getCanvas().localToScene(0, 0)).getX() + 2;
            triangleY.getPoints().setAll(0.0, 0.0, 8.0, 8.0, 8.0, -8.0);
        } else {
            axisPosY = getChart().getPlotForeground().sceneToLocal(getAxisY().getCanvas().localToScene(getAxisY().getWidth(), 0)).getX() - 2;
            triangleY.getPoints().setAll(0.0, 0.0, -8.0, 8.0, -8.0, -8.0);
        }
        final double yPosGlobal = getChart().getPlotForeground().sceneToLocal(getChart().getCanvas().localToScene(0, yPos)).getY();

        if (yPos < minY || yPos > maxY) {
            getChartChildren().clear();
        } else {
            layoutLabelY(new BoundingBox(minX, yPos, maxX - minX, 0), getLabelPositionX(), MIDDLE_POSITION);
            layoutLineY(minX, yPos, maxX, yPos);
            layoutMarkerY(axisPosY, yPosGlobal, minX, yPos);
        }

        final double xPos = minX + getAxisX().getDisplayPosition(getValueX());
        final double axisPos;
        if (getAxisX().getSide().equals(Side.BOTTOM)) {
            triangleX.getPoints().setAll(0.0, -8.0, -8.0, 0.0, 8.0, 0.0);
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxisX().getCanvas().localToScene(0, 0)).getY() + 6;
        } else {
            triangleX.getPoints().setAll(0.0, 0.0, -8.0, -8.0, 8.0, -8.0);
            axisPos = getChart().getPlotForeground().sceneToLocal(getAxisX().getCanvas().localToScene(0, getAxisX().getHeight())).getY() - 6;
        }
        final double xPosGlobal = getChart().getPlotForeground().sceneToLocal(getChart().getCanvas().localToScene(xPos, 0)).getX();

        if (xPos < minX || xPos > maxX) {
            getChartChildren().clear();
        } else {
            layoutLabelX(new BoundingBox(xPos, minY + 15, 0, maxY - minY), MIDDLE_POSITION, getLabelPositionY());
            layoutLineX(xPos, minY, xPos, maxY);
            layoutMarkerX(xPosGlobal, axisPos + 4, xPos, maxY);
        }
    }

    @Override
    public void updateStyleClass() {
        setStyleClasses(labelX, "x-", STYLE_CLASS_LABEL);
        setStyleClasses(labelY, "y-", STYLE_CLASS_LABEL);
        setStyleClasses(triangleY, "y-", STYLE_CLASS_MARKER);
        setStyleClasses(triangleX, "x-", STYLE_CLASS_MARKER);
    }

    @Override
    public Axis getAxis() {
        return null;
    }
}
