package KvetkaSpectrWithPelengsLib.CustomStringConverters;

import de.gsi.chart.axes.Axis;
import de.gsi.chart.axes.TickUnitSupplier;
import de.gsi.chart.axes.spi.format.AbstractFormatter;
import de.gsi.chart.axes.spi.format.DefaultTickUnitSupplier;
import de.gsi.chart.utils.NumberFormatter;
import javafx.util.StringConverter;

import java.text.DecimalFormat;
import java.text.ParseException;

public class CustomTickLabelFormatter extends AbstractFormatter {
        private static final TickUnitSupplier DEFAULT_TICK_UNIT_SUPPLIER = new DefaultTickUnitSupplier();
        private static final String FORMAT_SMALL_SCALE = "0.#";
        private static final String FORMAT_LARGE_SCALE = "0.0E0";
        public static final int DEFAULT_SMALL_LOG_AXIS = 8; // [orders of
        // magnitude], e.g. '4'
        // <-> [1,10000]
        private final DecimalFormat formatterSmall = new DecimalFormat(CustomTickLabelFormatter.FORMAT_SMALL_SCALE);
        private final DecimalFormat formatterLarge = new DecimalFormat(CustomTickLabelFormatter.FORMAT_LARGE_SCALE);
        private final MyCustomDecimalFormat formatter = new MyCustomDecimalFormat(formatterSmall);

        /**
         * Construct a DefaultFormatter for the given NumberAxis
         */
        public CustomTickLabelFormatter() {
            super();
            setTickUnitSupplier(CustomTickLabelFormatter.DEFAULT_TICK_UNIT_SUPPLIER);
        }

        /**
         * Construct a DefaultFormatter for the given NumberAxis
         *
         * @param axis The axis to format tick marks for
         */
        public CustomTickLabelFormatter(final Axis axis) {
            super(axis);
        }

        @Override
        public Number fromString(final String string) {
            return null;
        }

        @Override
        protected double getLogRange() {
            return Math.abs(Math.log10(rangeMin)) + Math.abs(Math.log10(rangeMax));
        }

        @Override
        protected void rangeUpdated() {
            final boolean smallScale = getLogRange() <= CustomTickLabelFormatter.DEFAULT_SMALL_LOG_AXIS;
            final DecimalFormat oldFormatter = formatter.getFormatter();

            if (smallScale) {
                formatter.setFormatter(formatterSmall);
                if (!formatter.getFormatter().equals(oldFormatter)) {
                    labelCache.clear();
                }
                return;
            }
            formatter.setFormatter(formatterLarge);
            if (!formatter.getFormatter().equals(oldFormatter)) {
                labelCache.clear();
            }
        }

        @Override
        public String toString(final Number object) {
            return labelCache.get(formatter, object.doubleValue());
        }

        private static class MyCustomDecimalFormat extends StringConverter<Number> implements NumberFormatter {
            private DecimalFormat localFormatter;

            public MyCustomDecimalFormat(final DecimalFormat formatter) {
                super();
                this.localFormatter = formatter;
            }

            @Override
            public Number fromString(String source) {
                try {
                    return localFormatter.parse(source);
                } catch (ParseException e) {
                    return Double.NaN;
                }
            }

            private DecimalFormat getFormatter() {
                return this.localFormatter;
            }

            @Override
            public int getPrecision() {
                return 0;
            }

            @Override
            public boolean isExponentialForm() {
                return false;
            }

            @Override
            public NumberFormatter setExponentialForm(boolean state) {
                return this;
            }

            private void setFormatter(final DecimalFormat formatter) {
                this.localFormatter = formatter;
            }

            @Override
            public NumberFormatter setPrecision(int precision) {
                return this;
            }

            @Override
            public String toString(double val) {
                return localFormatter.format(val);
            }

            @Override
            public String toString(Number object) {
                return localFormatter.format(object.doubleValue());
            }
        }
    }