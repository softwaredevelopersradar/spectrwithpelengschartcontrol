package KvetkaSpectrWithPelengsLib;

import KvetkaModels.AnalogReconFWSModel;
import KvetkaSpectrWithPelengsLib.Exceptions.NotFoundHeadException;
import KvetkaSpectrWithPelengsLib.Interface.ICallBackCommonChartEvents;
import KvetkaSpectrWithPelengsLib.Interface.ICallBackSignalEvents;
import de.gsi.dataset.utils.DoubleArrayCache;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

 public class Main extends Application implements ICallBackSignalEvents, ICallBackCommonChartEvents {

 int deley = 0;
double[] data;
     double[] dataf;
     int j =0;
 Pane_SpectrAndPelengs paneSpectrum;

     @Override
     public void start(Stage primaryStage) throws Exception{

         int size = 28000;
         paneSpectrum = new Pane_SpectrAndPelengs();
         primaryStage.setTitle("Hello World");
         primaryStage.setScene(new Scene(paneSpectrum, 1000, 900));
         primaryStage.setResizable(true);
         primaryStage.show();
         paneSpectrum.registerCallBackCommonChartEvents(this);
         paneSpectrum.registerCallBackSignalEvents(this);
         paneSpectrum.setMinPixelDistanceForReductionAlgorithm(0);
         paneSpectrum.setSelectEveryValueCount(1);
         //paneSpectrum.setAmountDisplayedOldPelengs(10);

        // paneSpectrum.getDrawingArea().addRangeHead(3000,10000);


         Runnable task = new Runnable() {
             final int size = 285000;
             @Override
             public void run() {
                 while (true){
                     double[] data2 = new double[size];
                     deley = 32;
                     try {
                         Thread.sleep(deley);
                     } catch (InterruptedException e) {
                         e.printStackTrace();
                     }

                     Platform.runLater(() -> {

                         for(int i = 0; i < size; i++)
                         {
                             data2[i] = ThreadLocalRandom.current().nextInt(90, 120);
                         }
                         double v1 = ThreadLocalRandom.current().nextInt(20,90);

                         for(int i = 0; i < 30; i++)
                         {
                             double v = Math.sin(Math.toRadians(i * 8)) * v1;
                             data2[size/2 + i] = ThreadLocalRandom.current().nextInt(80 - (int) v,100 - (int) v);
                         }

                         paneSpectrum.UpdateSpectr(data2, 1500, 30000);






                         double[] sticks = new double[20];
                         double b = 0;
                         for(int i = 0 ; i < sticks.length; i++)
                         {
                             double d = ThreadLocalRandom.current().nextDouble(0, 200);
                             b += 100 + d;
                             sticks[i] = 4000 + b;
                         }

                             paneSpectrum.getDrawingArea().setSticks(sticks,0);


                         double[] arrows = new double[10];

                         for(int i = 0 ; i < arrows.length; i++)
                         {
                             double d = ThreadLocalRandom.current().nextDouble(0, 300);
                             arrows[i] = 2500 + 2000*i + d;
                         }
                         paneSpectrum.getDrawingArea().setArrows(arrows);
                         paneSpectrum.getDrawingArea().IsSignalJamming(3,false);

                         System.gc();
                     });
                 }
             }
         };

         Thread thread = new Thread(task);
         thread.start();


         Runnable task1 = new Runnable() {
             @Override
             public void run() {
                 while (true){

                  data = new double[10];
                  dataf = new double[10];


                        data[0] = -10;
                        data[1] = -10;
                        data[2] = ThreadLocalRandom.current().nextDouble(120, 150);
                        data[3] = -10;
                        data[4] = -10;
                        data[5] = ThreadLocalRandom.current().nextDouble(220, 250);
                        data[6] = -10;
                        data[7] = -10;
                        data[8] = -10;
                        data[9] = -10;


                  j++;
                  if(j == 25)
                      j=0;

                     Platform.runLater(() -> {
                     paneSpectrum.UpdatePelengs(data,1000,4000 + 1000 * j);
                 });

                     try {
                         Thread.sleep(1000);
                     } catch (InterruptedException e) {
                         e.printStackTrace();
                     }

//                 List<Double> data1 = new ArrayList<>();
//                 for(int i = 0; i < size; i++)
//                 {
//                     //data.add(ThreadLocalRandom.current().nextDouble(0, 360));
//                    // data1.add(ThreadLocalRandom.current().nextDouble(1500, 7000));
//                     // if(i%2==0)
//                         data[i] = -10.;
//                 }
//                 data[ThreadLocalRandom.current().nextInt(100, 20000)] = ThreadLocalRandom.current().nextDouble(0, 360);
//                 data[ThreadLocalRandom.current().nextInt(100, 20000)] = ThreadLocalRandom.current().nextDouble(0, 360);
//                 data[ThreadLocalRandom.current().nextInt(100, 20000)] = ThreadLocalRandom.current().nextDouble(0, 360);
//                 data[ThreadLocalRandom.current().nextInt(100, 20000)] = ThreadLocalRandom.current().nextDouble(0, 360);
//
//                 Platform.runLater(() -> {
//                     paneSpectrum.UpdatePelengs(data, 1, 1500);
//                     DoubleArrayCache.getInstance().clear();
//                 });
//                 System.gc();
             }
         }};

         Thread thread1 = new Thread(task1);
         thread1.start();
     }


     public static void main(String[] args) {
         launch(args);
     }


     private void handleClick(ActionEvent actionEvent) {
         if(((ToggleButton)actionEvent.getSource()).isSelected())
         {
             System.out.println("Playing...");
         }
         else System.out.println("Stop playing");

     }

     private void handleClickRecord(ActionEvent actionEvent) {

         if(((ToggleButton)actionEvent.getSource()).isSelected())
         {
             System.out.println("Recording...");
         }
         else System.out.println("Stop recording");
     }
     @Override
     public void signalAdded(double frequency, int id, AnalogReconFWSModel analogReconFWSModel) {

         paneSpectrum.GetButtonObject(id).getPropertyPlayButton().set(this::handleClick);


         paneSpectrum.GetButtonObject(frequency).getPropertySoundButton().set(this::handleClickRecord);



         System.out.println("Added signal with frequency :" + frequency +
                 "\nId: " + id +
                 "\nDeltaF: " + analogReconFWSModel.getBand() +
                 "\nLevel: " + analogReconFWSModel.getLevel() +
                 "\n\n\n");
     }


     @Override
     public void signalDeleted(double frequency, int id, AnalogReconFWSModel analogReconFWSModel) {
         System.out.println("Deleted signal with frequency :" + frequency +
                 "\nId: " + id +
                 "\nDeltaF: " + analogReconFWSModel.getBand() +
                 "\nLevel: " + analogReconFWSModel.getLevel() +
                 "\n\n\n");
     }

     @Override
     public void signalBecameUnavailable(double frequency, int id, AnalogReconFWSModel analogReconFWSModel) {
         System.out.println("Unavailable signal with frequency :" + frequency +
                 "\nId: " + id +
                 "\nDeltaF: " + analogReconFWSModel.getBand() +
                 "\nLevel: " + analogReconFWSModel.getLevel() +
                 "\n\n\n");
     }

     @Override
     public void signalBecameAvailableAgain(double frequency, int id, AnalogReconFWSModel analogReconFWSModel) {
         System.out.println("Available signal with frequency :" + frequency +
                 "\nId: " + id +
                 "\nDeltaF: " + analogReconFWSModel.getBand() +
                 "\nLevel: " + analogReconFWSModel.getLevel() +
                 "\n\n\n");
     }

     @Override
     public void thresholdChanged(double threshold) {
         //System.out.println("Threshold changed on: " + threshold);
     }

 }
