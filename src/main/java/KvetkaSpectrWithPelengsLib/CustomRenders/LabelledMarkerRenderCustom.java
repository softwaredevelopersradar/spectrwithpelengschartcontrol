package KvetkaSpectrWithPelengsLib.CustomRenders;

import de.gsi.chart.XYChart;
import de.gsi.chart.XYChartCss;
import de.gsi.chart.axes.Axis;
import de.gsi.chart.renderer.spi.LabelledMarkerRenderer;
import de.gsi.chart.utils.StyleParser;
import de.gsi.dataset.DataSet;
import javafx.geometry.Orientation;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.Objects;

public class LabelledMarkerRenderCustom  extends LabelledMarkerRenderer {

    private boolean isShowDegrees = false;
    public void IsShowDegrees(boolean isShowDegrees)
    {
        this.isShowDegrees = isShowDegrees;
    }


    @Override
    protected void drawVerticalLabelledMarker(final GraphicsContext gc, final XYChart chart, final DataSet dataSet,
                                              final int indexMin, final int indexMax) {
        Axis xAxis = this.getFirstAxis(Orientation.HORIZONTAL);
        if (xAxis == null) {
            xAxis = chart.getFirstAxis(Orientation.HORIZONTAL);
        }

        Axis yAxis = this.getFirstAxis(Orientation.VERTICAL);
        if (yAxis == null) {
            yAxis = chart.getFirstAxis(Orientation.VERTICAL);
        }

        setGraphicsContextAttributes(gc, dataSet.getStyle());
        gc.setTextAlign(TextAlignment.LEFT);


        final double height = chart.getCanvas().getHeight();
        for (int i = 0; i < indexMax; i++) {
            final double screenX = (int) xAxis.getDisplayPosition(dataSet.get(DataSet.DIM_X, i));
            final double screenY = (int) yAxis.getDisplayPosition(dataSet.get(DataSet.DIM_Y, i));
            final String label = dataSet.getDataLabel(i);
            if (label == null) {
                continue;
            }


            gc.strokeLine(screenX, 0, screenX, height);
            gc.save();

            if(isShowDegrees)
            {
                double X = Math.ceil(screenX + 7);
                double Y = Math.ceil(screenY + 22);

                if(dataSet.get(DataSet.DIM_Y, i) <= 260)
                    Y = Math.ceil(screenY - 7);
                double d  = Math.round(this.getAxes().get(1).getMax());
                if(dataSet.get(DataSet.DIM_X,i) >= d - (d/25.0))
                    X = Math.ceil(screenX - 74);
                gc.translate(X,Y);
                gc.setFont(Font.font(15));
                gc.fillText(label, 0.0, 0);
                gc.restore();
            }
        }
    }

int j =0;

@Override
    protected void setGraphicsContextAttributes(final GraphicsContext gc, final String style) {
        gc.setStroke(Color.valueOf("#6c92fe"));

        final Color fillColor = StyleParser.getColorPropertyValue(style, XYChartCss.FILL_COLOR);
        if (fillColor == null) {
            gc.setFill(Color.valueOf("#6c92fe"));
        } else {
            gc.setFill(fillColor);
        }

        final Double strokeWidth = StyleParser.getFloatingDecimalPropertyValue(style, XYChartCss.STROKE_WIDTH);
        gc.setLineWidth(Objects.requireNonNullElseGet(strokeWidth, () -> strokeLineWidthMarker));

        final Font font = StyleParser.getFontPropertyValue(style);
            gc.setFont(font);

        gc.save();
    }

}
