package KvetkaSpectrWithPelengsLib;


import KvetkaSpectrWithPelengsLib.CustomIndicators.XRangeIndicatorCustom;
import KvetkaSpectrWithPelengsLib.CustomIndicators.XYValueIndicatorCustom;
import KvetkaSpectrWithPelengsLib.CustomIndicators.YValueIndicatorCustom;
import KvetkaSpectrWithPelengsLib.CustomRenders.LabelledMarkerRenderCustom;
import KvetkaSpectrWithPelengsLib.CustomZoomer.CustomZoomer;
import KvetkaSpectrWithPelengsLib.Drawing.DrawArea;
import de.gsi.chart.XYChart;
import de.gsi.chart.axes.AxisMode;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.plugins.ChartPlugin;
import de.gsi.chart.renderer.ErrorStyle;
import de.gsi.chart.renderer.datareduction.DefaultDataReducer;
import de.gsi.chart.renderer.spi.ErrorDataSetRenderer;
import de.gsi.chart.ui.geometry.Side;
import de.gsi.dataset.spi.DoubleDataSet;
import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;


public class OnlyChart_Spectr extends XYChart {
    private static final int BUFFER_CAPACITY_SPECTR = 300000;
    private static final int BUFFER_CAPACITY_PELENGS = 11000;
    private double maximumXValue = 30000;
    private double minimumXValue = 1500;
    private double maximumYValue = 30;
    private double minimumYValue = -120;
    private final double defaultXValueCrossIndicator = 10000;
    private final double defaultYValueCrossIndicator = -30;
    private final double thresholdIndicatorValue = -80;
    private int selectEvery_Count = 1;
    private boolean isDisplayFullSpectrum = true;

    private DrawArea drawArea;

    private final String axisYName = "Уровень";
    private final String axisYUnit = "Дб";
    private final String crossIndicatorXName = "Частота";
    private final String crossIndicatorYName = "Уровень";
    private final String thresholdIndicatorName= "Порог";

    public final DoubleDataSet dataBuffer = new DoubleDataSet("spectrum", BUFFER_CAPACITY_SPECTR);
    public final DoubleDataSet dataBuffer_Pelengs = new DoubleDataSet("pelengs", BUFFER_CAPACITY_PELENGS);
    private final ErrorDataSetRenderer dipoleCurrentRenderer = new ErrorDataSetRenderer();
    private final LabelledMarkerRenderCustom labelledMarkerRendered = new LabelledMarkerRenderCustom();
    private final CustomZoomer zoom = new CustomZoomer();;

    private YValueIndicatorCustom yValueIndicator;
    private XYValueIndicatorCustom xyValueIndicatorCustom;




    public OnlyChart_Spectr()
    {
        super(new DefaultNumericAxis(),new DefaultNumericAxis());
        initComponents();
    }


    public void setMaximumYValue(double maximumYValue){
        this.getYAxis().set(this.minimumYValue, maximumYValue);
        this.maximumYValue = maximumYValue;
        this.updateAxisRange();
    }

    public void setSelectEvery_Count(int count) {
        if(count == 0)
            this.selectEvery_Count = 1;
        else
        this.selectEvery_Count = count; }


    public void setIsDisplayFullSpectrum(boolean displayFullSpectrum) { isDisplayFullSpectrum = displayFullSpectrum;}

    public void setZoomRange(double leftZoomBound, double rightZoomBound) {zoom.setZoomRange(leftZoomBound, rightZoomBound);}

    public void setZoomDepth(double zoomDepth) {zoom.setZoomDepth(zoomDepth);}

    public double getMaximumXValue() { return this.maximumXValue; }

    public double getMinimumXValue() { return this.minimumXValue; }

    public DrawArea getDrawArea() {return drawArea; }

    public DoubleProperty getValueProperty_Porog()
    {
        return yValueIndicator.valueProperty();
    }

    public DoubleProperty getValuePropertyY_Cross()
    {
        return xyValueIndicatorCustom.valuePropertyY();
    }

    public DoubleProperty getValuePropertyX_Cross()
    {
        return xyValueIndicatorCustom.valuePropertyX();
    }

    public double getValue_Porog()
    {
        return yValueIndicator.getValue();
    }

    public double getValueX_Cross()
    {
        return xyValueIndicatorCustom.getValueX();
    }

    public double getValueY_Cross()
    {
        return xyValueIndicatorCustom.getValueY();
    }
    public double[] getXList() { return this.getDataSetForAxis(this.getXAxis()).get(1).getValues(0); }

    public double[] getYList() { return this.getDataSetForAxis(this.getYAxis()).get(1).getValues(1); }

    /**
     * get point in plot coordinates (frequency, level)
     *
     * @param x x coordinate of mouse click in the canvas coordinate system
     * @param y y coordinate of mouse click in the canvas coordinate system
     * @return point in plot coordinates (frequency, level)
     */
    public Point2D getValuePosFromMouseClick(double x, double y)
    {
        Point2D c = this.getPlotArea().sceneToLocal(x, y);
        final double yPosData = this.getYAxis().getValueForDisplay(c.getY());
        final double xPosData = this.getXAxis().getValueForDisplay(c.getX());
        return new Point2D(xPosData,yPosData);
    }

    /**
     * displays a new dataset
     *
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(double[] dataSpectr) {
        MassOfPoints massOfPoints = null;
        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(minimumXValue, maximumXValue, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(minimumXValue, maximumXValue, dataSpectr);


        if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }

    /**
     *  displays a new dataset
     *
     * @param endFreq end frequency of the displayed spectrum
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(double endFreq, double[] dataSpectr) {
        MassOfPoints massOfPoints = null;
        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(minimumXValue,endFreq, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(minimumXValue,endFreq, dataSpectr);

        if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }

    /**
     * displays a new dataset
     *
     * @param startFreq start frequency of the displayed spectrum
     * @param endFreq end frequency of the displayed spectrum
     * @param dataSpectr array of level values
     */
    public void PlotSpectr(double startFreq, double endFreq, double[] dataSpectr) {
        MassOfPoints massOfPoints = null;
        if(isDisplayFullSpectrum)
        {
            massOfPoints = createFullSpectrData(startFreq,endFreq, dataSpectr);
            dataBuffer.clearData();
        }
        else massOfPoints = createSpectrData(startFreq,endFreq, dataSpectr);

        if(massOfPoints != null)
            dataBuffer.set(massOfPoints.startIndex, massOfPoints.X, massOfPoints.Y);
    }


    public void PlotPelengs(double[] freq) {
        dataBuffer_Pelengs.clearData();
        double[] yy = new double[freq.length];

        dataBuffer_Pelengs.add(freq,yy);
        for(int i = 0; i < freq.length; i++)
        {
            dataBuffer_Pelengs.addDataLabel(i,"Пеленг " + (1 + i));
        }
    }

    /**
     *  change color of selected signal indicator. Identifier frequency
     */
    public void changeColorXRangeIndicator(double freq)
    {
        for (ChartPlugin plugin : this.getPlugins()){
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                if(((XRangeIndicatorCustom) plugin).getLowerBound() <= freq && ((XRangeIndicatorCustom) plugin).getUpperBound() >= freq)
                    ((XRangeIndicatorCustom) plugin).setColor(Color.RED);
                else ((XRangeIndicatorCustom) plugin).setColor(Color.BLUE);
            }
        }
    }

    /**
     *  deletes the indicator of the selected signal
     */
    public void deleteRangeIndicator(double freq)
    {
        ObservableList<ChartPlugin> chartPluginObservableList = FXCollections.observableArrayList(this.getPlugins());
        for (ChartPlugin plugin : chartPluginObservableList){
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                if(((XRangeIndicatorCustom) plugin).getLowerBound() <= freq && ((XRangeIndicatorCustom) plugin).getUpperBound() >= freq)
                    this.getPlugins().remove(plugin);
            }
        }
    }

    /**
     *  added the indicator of the selected signal
     *
     * @param startRange frequency of the left edge of the range indicator
     * @param endRange frequency of the right edge of the range indicator
     */
    public void addXRangeIndicator(double startRange, double endRange)
    {
        XRangeIndicatorCustom xRangeIndicatorCustom;
        xRangeIndicatorCustom = new XRangeIndicatorCustom(this.getXAxis(), startRange, endRange);
        xRangeIndicatorCustom.setEditable(true);
        changeColorAllXRangeIndicator();
        xRangeIndicatorCustom.setColor(Color.RED);
        this.getPlugins().add(xRangeIndicatorCustom);
    }

    /**
     * change min pixel distance for reduction algorithm
     *
     * @param minPixelDistance The larger the value, the faster the output works, the smaller the more correct the values will be displayed
     */
    public void setMinPixelDistanceForReductionAlgorithm(int minPixelDistance)// чем больше значение, тем быстрее отображение, но могут теряться точки
    {
        ((DefaultDataReducer) dipoleCurrentRenderer.getRendererDataReducer()).setMinPointPixelDistance(minPixelDistance);
    }


    private void initComponents() {
        this.setAnimated(false);
        this.setLegendVisible(false);

        this.getXAxis().getCanvas().setVisible(false);
        this.getXAxis().invertAxis(false);
        this.getXAxis().set(minimumXValue, maximumXValue);
        this.getXAxis().setAutoRanging(false);

        this.getYAxis().setName(axisYName);
        this.getYAxis().setUnit(axisYUnit);
        this.getYAxis().setAutoRanging(false);
        this.getYAxis().setSide(Side.LEFT);
        this.getYAxis().set(minimumYValue,maximumYValue);


        zoom.setSliderVisible(false);
        zoom.setAxisMode(AxisMode.X);
        zoom.setAddButtonsToToolBar(false);
        zoom.setZoomInMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED && mouseEvent.getClickCount() == 1);
        zoom.setZoomOriginMouseFilter(mouseEvent -> mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.getClickCount() == 2);
        zoom.setZoomOutMouseFilter(mouseEvent -> false);
        this.getPlugins().add(zoom);


        initLabelledDataSetRender(labelledMarkerRendered);
        initErrorDataSetRenderer(dipoleCurrentRenderer);

        dataBuffer.setStyle("strokeColor=" + Color.rgb(255,201,0)+";" +"strokeWidth=1.5;");
        dataBuffer_Pelengs.setStyle("fillColor=" + Color.rgb(108,146,254)+";");

        drawArea = new DrawArea(this);

        yValueIndicator = new YValueIndicatorCustom(this.getYAxis(), thresholdIndicatorValue, thresholdIndicatorName);
        yValueIndicator.setColor(Color.RED);
        this.getPlugins().add(yValueIndicator);

        xyValueIndicatorCustom = new XYValueIndicatorCustom(this.getXAxis(), this.getYAxis(), defaultXValueCrossIndicator, defaultYValueCrossIndicator, this.getPlotArea(),crossIndicatorXName, crossIndicatorYName );
        this.getPlugins().add(xyValueIndicatorCustom);
    }


    private void initErrorDataSetRenderer(final ErrorDataSetRenderer eRenderer) {
        eRenderer.setErrorType(ErrorStyle.NONE);
        eRenderer.setDrawMarker(false);
        eRenderer.getAxes().addAll(this.getYAxis());
        this.getRenderers().add(eRenderer);
        eRenderer.getDatasets().add(dataBuffer);
    }
    private void initLabelledDataSetRender(final LabelledMarkerRenderCustom eRenderer) {
        eRenderer.getAxes().addAll(this.getYAxis());
        eRenderer.getDatasets().add(dataBuffer_Pelengs);
        eRenderer.enableVerticalMarker(true);
        this.getRenderers().add(eRenderer);
    }

    private void changeColorAllXRangeIndicator()
    {
        for (ChartPlugin plugin : this.getPlugins()) {
            if(plugin.getClass() == XRangeIndicatorCustom.class)
            {
                ((XRangeIndicatorCustom) plugin).setColor(Color.BLUE);
            }
        }
    }


    private MassOfPoints createFullSpectrData(double startFreq, double endFreq, double[] dataSpectr)
    {
        double y = (endFreq-startFreq) / dataSpectr.length;

        int length = dataSpectr.length / selectEvery_Count;

        double[] xx = new double[length];
        double[] yy = new double[length];

        for (int i = 0, j = 0; i < dataSpectr.length; i += selectEvery_Count, j++) {
            xx[j] = y * i + startFreq;
            yy[j] = (-1)*dataSpectr[i];
        }

        return new MassOfPoints(0, xx, yy);
    }


    private MassOfPoints createSpectrData(double startFreq, double endFreq, double[] dataSpectr)
    {
        double y = (endFreq-startFreq) / dataSpectr.length;
        int length = dataSpectr.length / selectEvery_Count;
        double[] xx = new double[length];
        double[] yy = new double[length];
        int ind = 0;

        try{
            ind = this.getAllDatasets().get(0).getIndex(0,startFreq);

            if(this.getAllDatasets().get(0).getValue(0,ind) < startFreq && this.getAllDatasets().get(0).getDataCount() > 5)
                ind++;

            for (int i = 0, j = 0; i < dataSpectr.length; i += selectEvery_Count, j++) {
                xx[j] = y * i + startFreq;
                yy[j] = ((-1)*(dataSpectr[i]));
            }
        }
        catch (Exception ex)
        {
            return null;
        }

        return new MassOfPoints(ind, xx, yy);
    }
}

class MassOfPoints
{
    public MassOfPoints(int size)
    {
        X = new double[size];
        Y = new double[size];
    }

    public MassOfPoints(int startIndex, double[] X, double[] Y)
    {
        this.startIndex = startIndex;
        this.X = X;
        this.Y = Y;
    }

    int startIndex = 0;
    double[] X;
    double[] Y;
}