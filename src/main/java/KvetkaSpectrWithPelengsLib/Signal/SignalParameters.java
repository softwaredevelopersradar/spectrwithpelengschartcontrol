package KvetkaSpectrWithPelengsLib.Signal;

import KvetkaModels.AnalogReconFWSModel;
import KvetkaModels.Coord;

public class SignalParameters {

    private int[] dataSet;

    private int indexAtWhichSelectedAreaStarts;
    private int indexAtWhichSelectedAreaEnds;
    private double frequencyAtWhichSelectedAreaStarts;
    private double frequencyAtWhichSelectedAreaEnds;

    private boolean isSignalExist = false;


    private boolean isSignalPreviouslyDetected;

    private AnalogReconFWSModel analogReconFWSModel;

    /**
     * Create new instance of <code>SignalParameters<code/>
     *
     *
     * @param dataSet array of level values
     * @param indexAtWhichSelectedAreaStarts index of the level array corresponding to the beginning of the copied part of the spectrum
     * @param indexAtWhichSelectedAreaEnds index of the level array corresponding to the end of the copied part of the spectrum
     * @param frequencyAtWhichSelectedAreaStarts starting frequency of the copied part of the spectrum
     * @param frequencyAtWhichSelectedAreaEnds end frequency of the copied part of the spectrum
     * @param isSignalAvailable shows whether the signal is being monitored
     */
    public SignalParameters(double[] dataSet, int indexAtWhichSelectedAreaStarts, int indexAtWhichSelectedAreaEnds, double frequencyAtWhichSelectedAreaStarts, double frequencyAtWhichSelectedAreaEnds, boolean isSignalAvailable) {
        this.dataSet = SignalMath.ConvertDoubleMassiveToInt(dataSet);
        this.indexAtWhichSelectedAreaStarts = indexAtWhichSelectedAreaStarts;
        this.indexAtWhichSelectedAreaEnds = indexAtWhichSelectedAreaEnds;
        this.frequencyAtWhichSelectedAreaStarts = frequencyAtWhichSelectedAreaStarts;
        this.frequencyAtWhichSelectedAreaEnds = frequencyAtWhichSelectedAreaEnds;
        this.isSignalExist = isSignalAvailable;
    }


    public void setDataSet(int[] dataSet) {
        this.dataSet = dataSet;
    }

    public void setSignalExist(boolean signalExist) {
        isSignalExist = signalExist;
        if(signalExist)
            isSignalPreviouslyDetected = true;
    }

    public void setIndexAtWhichSelectedAreaStarts(int indexAtWhichSelectedAreaStarts) {
        this.indexAtWhichSelectedAreaStarts = indexAtWhichSelectedAreaStarts;
    }

    public void setIndexAtWhichSelectedAreaEnds(int indexAtWhichSelectedAreaEnds) {
        this.indexAtWhichSelectedAreaEnds = indexAtWhichSelectedAreaEnds;
    }

    public void setFrequencyAtWhichSelectedAreaStarts(double frequencyAtWhichSelectedAreaStarts) {
        this.frequencyAtWhichSelectedAreaStarts = frequencyAtWhichSelectedAreaStarts;
    }

    public void setFrequencyAtWhichSelectedAreaEnds(double frequencyAtWhichSelectedAreaEnds) {
        this.frequencyAtWhichSelectedAreaEnds = frequencyAtWhichSelectedAreaEnds;
    }


    public AnalogReconFWSModel getAnalogReconFWSModel()
    {
        analogReconFWSModel = new AnalogReconFWSModel();
        analogReconFWSModel.setFrequency(getFrequency());
        analogReconFWSModel.setBandDemodulation(8000);
        analogReconFWSModel.setTypeSignal("AM");
        analogReconFWSModel.setCoordinates(new Coord(-1, -1, 0));

        if(isSignalExist == true){
            analogReconFWSModel.setBand((float)getDeltaF());
            analogReconFWSModel.setLevel((int)getMaxLevel());
        }
        else
        {
            analogReconFWSModel.setBand(0.0f);
            analogReconFWSModel.setLevel(0);
        }
       return analogReconFWSModel;
    }



    public boolean isSignalExist() {
        return isSignalExist;
    }

    public boolean isSignalPreviouslyDetected() {return isSignalPreviouslyDetected;}

    /**
     * gives the maximum level found in the array
     *
     * @return maximum level found in the array
     */
    public double getMaxLevel() {
        return FindMaxLevel();
    }

    /**
     * gives the frequency corresponding to the maximum level
     *
     * @param porog Threshold
     * @return frequency corresponding to the maximum level
     */
    public double getFrequency(double porog) {
        return CalculateFrequency(porog);
    }

    /**
     * gives the frequency corresponding to the maximum level
     *
     * @return frequency corresponding to the maximum level
     */
    public double getFrequency()
    {
        return CalculateFrequency();
    }

    /**
     * gives the delta F for that signal
     *
     * @return delta F for that signal
     */
    public double getDeltaF() {
        return CalculateDeltaFForSignal();
    }

    /**
     * gives array of level values
     *
     * @return array of level values
     */
    public int[] getDataSet() {
        return dataSet;
    }


    /**
     * gives index of the level array corresponding to the end of the copied part of the spectrum
     *
     * @return index of the level array corresponding to the end of the copied part of the spectrum
     */
    public int getIndexAtWhichSelectedAreaEnds() {
        return indexAtWhichSelectedAreaEnds;
    }

    /**
     * gives index of the level array corresponding to the beginning of the copied part of the spectrum
     *
     * @return index of the level array corresponding to the beginning of the copied part of the spectrum
     */
    public int getIndexAtWhichSelectedAreaStarts() {
        return indexAtWhichSelectedAreaStarts;
    }

    /**
     * gives starting frequency of the copied part of the spectrum
     *
     * @return starting frequency of the copied part of the spectrum
     */
    public double getFrequencyAtWhichSelectedAreaStarts() {
        return frequencyAtWhichSelectedAreaStarts;
    }

    /**
     * gives end frequency of the copied part of the spectrum
     *
     * @return end frequency of the copied part of the spectrum
     */
    public double getFrequencyAtWhichSelectedAreaEnds() {
        return frequencyAtWhichSelectedAreaEnds;
    }


    /**
     * returns the frequency at the center of the selected range
     *
     * @return frequency at the center of the selected range
     */
    public  double getCenterFrequency() {return ((frequencyAtWhichSelectedAreaEnds - frequencyAtWhichSelectedAreaStarts)/2) + frequencyAtWhichSelectedAreaStarts; }


    private double CalculateFrequency(double porog)
    {
        return SignalMath.CalculateFrequency(dataSet, frequencyAtWhichSelectedAreaStarts, frequencyAtWhichSelectedAreaEnds, porog);
    }

    private double CalculateFrequency()
    {
        return SignalMath.CalculateFrequency(dataSet, frequencyAtWhichSelectedAreaStarts, frequencyAtWhichSelectedAreaEnds);
    }

    private double FindMaxLevel()
    {
        return SignalMath.FindMaxLevel(dataSet);
    }

    private double CalculateDeltaFForSignal()
    {
        return SignalMath.CalculateDeltaFForSignal(dataSet, frequencyAtWhichSelectedAreaStarts, frequencyAtWhichSelectedAreaEnds);
    }
}
