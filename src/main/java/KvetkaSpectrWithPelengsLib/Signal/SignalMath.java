package KvetkaSpectrWithPelengsLib.Signal;

public class SignalMath {


    public static int[] ConvertDoubleMassiveToInt(double[] dataSet)
    {
        int[] newIntDataSet = new int[dataSet.length];
        for(int i = 0; i < newIntDataSet.length; i++)
        {
            try
            {
            newIntDataSet[i] = (int) dataSet[i];
            }catch (Exception ex)
            {
                newIntDataSet[i] = 120;
            }
        }
        return  newIntDataSet;
    }

    private static void absOfMassive(int[] newDataSet)
    {
        if(newDataSet[10] <=0)
        {
            for(int i = 0; i < newDataSet.length; i++)
                newDataSet[i] = Math.abs(newDataSet[i]);
        }
    }


    public static double CalculateFrequency(int[] newDataSet, double startFrequency, double endFrequency, double porog)
    {
        absOfMassive(newDataSet);
        double maxLevelIndex = FindMaxLevelIndex(newDataSet, porog);
        return Math.round((startFrequency + maxLevelIndex * (Math.abs(endFrequency - startFrequency)/newDataSet.length))*100000.)/100000.;
    }

    public static double CalculateFrequency(int[] newDataSet, double startFrequency, double endFrequency)
    {
        absOfMassive(newDataSet);
        double maxLevelIndex = FindMaxLevelIndex(newDataSet,256);
        return Math.round((startFrequency + maxLevelIndex * (Math.abs(endFrequency - startFrequency)/newDataSet.length))*100000.)/100000.;
    }

    public static double FindMaxLevel(int[] newDataSet)
    {
        absOfMassive(newDataSet);
        double maxLevelValue = newDataSet[0];
        for (int b : newDataSet) {
            if (b < maxLevelValue && b > 0) {
                maxLevelValue = b;
            }
        }
        return (-1) * maxLevelValue;
    }

    public static double FindMaxLevelIndex(int[] newDataSet, double porog)
    {
        absOfMassive(newDataSet);
        double maxLevelValue = newDataSet[0];
        int  indexFirstAppearMaxVerticalValue = 0;
        int  indexSecondAppearMaxVerticalValue = 0;
        for(int i = 0; i < newDataSet.length; i++)
        {
            if (newDataSet[i] < maxLevelValue && newDataSet[i] > 0) {
                maxLevelValue = newDataSet[i];
                indexFirstAppearMaxVerticalValue = i;
            }
        }

        indexSecondAppearMaxVerticalValue = indexFirstAppearMaxVerticalValue;
        for(int i = indexFirstAppearMaxVerticalValue; i < newDataSet.length; i++)
        {
            if(newDataSet[i] > maxLevelValue)
            {
                indexSecondAppearMaxVerticalValue = i - 1;
                break;
            }
        }

        if(maxLevelValue >= Math.abs(porog))
            return newDataSet.length / 2;

        return indexFirstAppearMaxVerticalValue + (Math.abs(indexSecondAppearMaxVerticalValue - indexFirstAppearMaxVerticalValue) / 2.);
    }

    public static double CalculateDeltaFForSignal(int[] newDataSet, double startFrequency, double endFrequency)
    {
        absOfMassive(newDataSet);
        int leftIndexOfDeltaF = 0;
        int rightIndexOfDeltaF = 0;
        final double maxLevelValue = Math.abs(FindMaxLevel(newDataSet));
        final int indexOfMaxVerticalValue = (int) FindMaxLevelIndex(newDataSet, 256);

        for(int i = indexOfMaxVerticalValue; i > 0; i--)
        {
            if(newDataSet[i] > (maxLevelValue + 3))
            {
                leftIndexOfDeltaF = i+1;
                break;
            }
        }

        for(int i = indexOfMaxVerticalValue; i < newDataSet.length; i++)
        {
            if(newDataSet[i] > (maxLevelValue + 3))
            {
                rightIndexOfDeltaF = i-1;
                break;
            }
        }

        double deltaFValue = 0.0;
        final double deltaFrequency = Math.abs(endFrequency - startFrequency) / newDataSet.length;
        if(newDataSet[leftIndexOfDeltaF] != maxLevelValue + 3)
        {
            if(leftIndexOfDeltaF > 0){
                double P1x = leftIndexOfDeltaF;
                double P1y = newDataSet[leftIndexOfDeltaF];
                double P2x = leftIndexOfDeltaF - 1;
                double P2y = newDataSet[leftIndexOfDeltaF - 1];

                final double deltaFValueLeftPartIndex = (((P2x-P1x) * ((maxLevelValue + 3) - P1y))/(P2y - P1y) + P1x);

                P1x = rightIndexOfDeltaF;
                P2x = rightIndexOfDeltaF + 1;
                P1y = newDataSet[rightIndexOfDeltaF];
                P2y = newDataSet[rightIndexOfDeltaF+1];

                final double deltaFValueRightPartIndex = (((P2x-P1x) * ((maxLevelValue + 3) - P1y))/(P2y - P1y) + P1x);
                deltaFValue = (deltaFValueRightPartIndex - deltaFValueLeftPartIndex) * deltaFrequency;
            }
        }
        return deltaFValue;
    }
}
