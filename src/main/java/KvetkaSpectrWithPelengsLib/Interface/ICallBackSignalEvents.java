package KvetkaSpectrWithPelengsLib.Interface;

import KvetkaModels.AnalogReconFWSModel;

/**
 * makes it possible to process events related to signal tracking
 */
public interface ICallBackSignalEvents {
    /**
     * occurs if a new signal is added to tracking
     *
     * @param frequency           frequency of the signal
     * @param id                  id of the signal
     * @param analogReconFWSModel information about signal
     */
    public void signalAdded(double frequency, int id, AnalogReconFWSModel analogReconFWSModel);

    /**
     * occurs if some tracked signal is removed
     *
     * @param frequency           frequency of the signal
     * @param id                  id of the signal
     * @param analogReconFWSModel information about signal
     */
    public void signalDeleted(double frequency, int id, AnalogReconFWSModel analogReconFWSModel);

    /**
     * occurs if some tracked signal is missing
     *
     * @param frequency           frequency of the signal
     * @param id                  id of the signal
     * @param analogReconFWSModel information about signal
     */
    public void signalBecameUnavailable(double frequency, int id, AnalogReconFWSModel analogReconFWSModel);

    /**
     * occurs if some tracked signal appears again
     *
     * @param frequency           frequency of the signal
     * @param id                  id of the signal
     * @param analogReconFWSModel information about signal
     */
    public void signalBecameAvailableAgain(double frequency, int id, AnalogReconFWSModel analogReconFWSModel);
}
