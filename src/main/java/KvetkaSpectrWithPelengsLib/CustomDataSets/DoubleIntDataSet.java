package KvetkaSpectrWithPelengsLib.CustomDataSets;

import de.gsi.dataset.AxisDescription;
import de.gsi.dataset.DataSet;
import de.gsi.dataset.DataSet2D;
import de.gsi.dataset.EditableDataSet;
import de.gsi.dataset.event.AddedDataEvent;
import de.gsi.dataset.event.EventListener;
import de.gsi.dataset.event.RemovedDataEvent;
import de.gsi.dataset.event.UpdatedDataEvent;
import de.gsi.dataset.locks.DataSetLock;
import de.gsi.dataset.spi.AbstractDataSet;
import de.gsi.dataset.utils.AssertUtils;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class DoubleIntDataSet extends AbstractDataSet<DoubleIntDataSet> implements EditableDataSet, DataSet2D {
    private static final long serialVersionUID = -493232313124620828L;
    private static final String X_COORDINATES = "X coordinates";
    private static final String Y_COORDINATES = "Y coordinates";
    protected DoubleArrayList xValues; // way faster than java default lists
    protected IntArrayList yValues; // way faster than java default lists

    /**
     * Creates a new instance of <code>DoubleByteDataSet</code> as copy of another (deep-copy).
     *
     * @param another name of this DataSet.
     */
    public DoubleIntDataSet(final DataSet another) {
        super(another.getName(), another.getDimension());
        set(another); // NOPMD by rstein on 25/06/19 07:42
    }

    /**
     * Creates a new instance of <code>DoubleByteDataSet</code>.
     *
     * @param name name of this DataSet.
     * @throws IllegalArgumentException if {@code name} is {@code null}
     */
    public DoubleIntDataSet(final String name) {
        this(name, 0);
    }

    /**
     * <p>
     * Creates a new instance of <code>DoubleByteDataSet</code>.
     * </p>
     * The user than specify via the copy parameter, whether the dataset wraps the input arrays themselves or on a
     * copies of the input arrays.
     *
     * @param name name of this data set.
     * @param xValues X coordinates
     * @param yValues Y coordinates
     * @param initalSize how many data points are relevant to be taken
     * @param deepCopy if true, the input array is copied
     * @throws IllegalArgumentException if any of the parameters is {@code null} or if arrays with coordinates have
     *             different lengths
     */
    public DoubleIntDataSet(final String name, final double[] xValues, final double[] yValues, final int initalSize,
                            final boolean deepCopy) {
        super(name, 2);
        //set(xValues, yValues, initalSize, deepCopy); // NOPMD
    }

    /**
     * Creates a new instance of <code>DoubleByteDataSet</code>.
     *
     * @param name name of this DataSet.
     * @param initalSize initial capacity of buffer (N.B. size=0)
     * @throws IllegalArgumentException if {@code name} is {@code null}
     */
    public DoubleIntDataSet(final String name, final int initalSize) {
        super(name, 2);
        AssertUtils.gtEqThanZero("initalSize", initalSize);
        xValues = new DoubleArrayList(initalSize);
        yValues = new IntArrayList(initalSize);
    }

    public DoubleIntDataSet add(final double x, final byte y) {
        return add(this.getDataCount(), x, y, null);
    }

    /**
     * Add point to the data set.
     *
     * @param x horizontal coordinate of the new data point
     * @param y vertical coordinate of the new data point
     * @param label the data label
     * @return itself (fluent design)
     */
    public DoubleIntDataSet add(final double x, final int y, final String label) {
        lock().writeLockGuard(() -> {
            xValues.add(x);
            yValues.add(y);

            if ((label != null) && !label.isEmpty()) {
                addDataLabel(xValues.size() - 1, label);
            }

            getAxisDescription(DIM_X).add(x);
            getAxisDescription(DIM_Y).add(y);
        });
        return fireInvalidated(new UpdatedDataEvent(this, "add"));
    }

    /**
     * Add array vectors to data set.
     *
     * @param xValuesNew X coordinates
     * @param yValuesNew Y coordinates
     * @return itself
     */
    public DoubleIntDataSet add(final double[] xValuesNew, final int[] yValuesNew) {
        AssertUtils.notNull(X_COORDINATES, xValuesNew);
        AssertUtils.notNull(Y_COORDINATES, yValuesNew);

        lock().writeLockGuard(() -> {
            final int addAt = xValues.size();
            final int newElements = Math.min(xValuesNew.length, yValuesNew.length);
            resize(addAt + newElements);
            xValues.setElements(addAt, xValuesNew);
            yValues.setElements(addAt, yValuesNew);

        });

        return fireInvalidated(new AddedDataEvent(this));
    }

    /**
     * need for implemented interface
     */
    @Override
    public DoubleIntDataSet add(final int index, final double... newValue) {
        return null;
        //return add(index, newValue[0], newValue[1], null);
    }

    /**
     * add point to the data set
     *
     * @param index data point index at which the new data point should be added
     * @param x horizontal coordinate of the new data point
     * @param y vertical coordinate of the new data point
     * @return itself (fluent design)
     */
    public DoubleIntDataSet add(final int index, final double x, final int y) {
        return add(index, x, y, null);
    }

    /**
     * add point to the data set
     *
     * @param index data point index at which the new data point should be added
     * @param x horizontal coordinates of the new data point
     * @param y vertical coordinates of the new data point
     * @param label data point label (see CategoryAxis)
     * @return itself (fluent design)
     */
    public DoubleIntDataSet add(final int index, final double x, final int y, final String label) {
        lock().writeLockGuard(() -> {
            final int indexAt = Math.max(0, Math.min(index, getDataCount() + 1));

            xValues.add(indexAt, x);
            yValues.add(indexAt, y);
            getDataLabelMap().addValueAndShiftKeys(indexAt, xValues.size(), label);
            getDataStyleMap().shiftKeys(indexAt, xValues.size());
            getAxisDescription(DIM_X).add(x);
            getAxisDescription(DIM_Y).add(y);
        });
        return fireInvalidated(new AddedDataEvent(this));
    }

    /**
     * add point to the data set
     *
     * @param index data point index at which the new data point should be added
     * @param x horizontal coordinate of the new data point
     * @param y vertical coordinate of the new data point
     * @return itself (fluent design)
     */
    public DoubleIntDataSet add(final int index, final double[] x, final int[] y) {
        AssertUtils.notNull(X_COORDINATES, x);
        AssertUtils.notNull(Y_COORDINATES, y);
        final int min = Math.min(x.length, y.length);

        lock().writeLockGuard(() -> {
            final int indexAt = Math.max(0, Math.min(index, getDataCount() + 1));
            xValues.addElements(indexAt, x, 0, min);
            yValues.addElements(indexAt, y, 0, min);
            getDataLabelMap().shiftKeys(indexAt, xValues.size());
            getDataStyleMap().shiftKeys(indexAt, xValues.size());
        });
        return fireInvalidated(new AddedDataEvent(this));
    }

    /**
     * clear all data points
     *
     * @return itself (fluent design)
     */
    public DoubleIntDataSet clearData() {
        lock().writeLockGuard(() -> {
            xValues.clear();
            yValues.clear();
            getDataLabelMap().clear();
            getDataStyleMap().clear();
            clearMetaInfo();

            getAxisDescriptions().forEach(AxisDescription::clear);
        });
        return fireInvalidated(new RemovedDataEvent(this, "clearData()"));
    }

    @Override
    public final double get(final int dimIndex, final int index) {
        return dimIndex == DataSet.DIM_X ? xValues.elements()[index] : yValues.elements()[index];
    }

    /**
     * @return storage capacity of dataset
     */
    public int getCapacity() {
        return Math.min(xValues.elements().length, yValues.elements().length);
    }

    @Override
    public int getDataCount() {
        return Math.min(xValues.size(), yValues.size());
    }

    @Override
    public DataSet set(DataSet other, boolean copy) {
        return new DataSet() {
            @Override
            public double get(int dimIndex, int index) {
                return 0;
            }

            @Override
            public List<AxisDescription> getAxisDescriptions() {
                return null;
            }

            @Override
            public int getDataCount() {
                return 0;
            }

            @Override
            public String getDataLabel(int index) {
                return null;
            }

            @Override
            public int getDimension() {
                return 0;
            }

            @Override
            public int getIndex(int dimIndex, double... x) {
                return 0;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getStyle() {
                return null;
            }

            @Override
            public String getStyle(int index) {
                return null;
            }

            @Override
            public double[] getValues(int dimIndex) {
                return new double[0];
            }

            @Override
            public <D extends DataSet> DataSetLock<D> lock() {
                return null;
            }

            @Override
            public DataSet recomputeLimits(int dimIndex) {
                return null;
            }

            @Override
            public DataSet setStyle(String style) {
                return null;
            }

            @Override
            public double getValue(int dimIndex, double... x) {
                return 0;
            }

            @Override
            public DataSet set(DataSet other, boolean copy) {
                return null;
            }

            @Override
            public AtomicBoolean autoNotification() {
                return null;
            }

            @Override
            public List<EventListener> updateEventListener() {
                return null;
            }
        };
    }


    public final double[] getValuesX(final int dimIndex) {
        return xValues.elements();
    }


    public final int[] getValuesY(final int dimIndex) {
        return yValues.elements();
    }

    /**
     * @param amount storage capacity increase
     * @return itself (fluent design)
     */
    public DoubleIntDataSet increaseCapacity(final int amount) {
        lock().writeLockGuard(() -> {
            final int size = getDataCount();
            resize(getCapacity() + amount);
            resize(size);
        });
        return getThis();
    }

    /**
     * remove point from data set
     *
     * @param index data point which should be removed
     * @return itself (fluent design)
     */
    @Override
    public EditableDataSet remove(final int index) {
        return remove(index, index + 1);
    }

    /**
     * removes sub-range of data points
     *
     * @param fromIndex start index
     * @param toIndex stop index
     * @return itself (fluent design)
     */
    public DoubleIntDataSet remove(final int fromIndex, final int toIndex) {
        lock().writeLockGuard(() -> {
            AssertUtils.indexInBounds(fromIndex, getDataCount(), "fromIndex");
            AssertUtils.indexOrder(fromIndex, "fromIndex", toIndex, "toIndex");

            final int clampedToIndex = Math.min(toIndex, getDataCount());
            xValues.removeElements(fromIndex, clampedToIndex);
            yValues.removeElements(fromIndex, clampedToIndex);

            // remove old label and style keys
            getDataLabelMap().remove(fromIndex, clampedToIndex);
            getDataStyleMap().remove(fromIndex, clampedToIndex);

            // invalidate ranges
            getAxisDescriptions().forEach(AxisDescription::clear);
        });
        return fireInvalidated(new RemovedDataEvent(this));
    }

    /**
     * ensures minimum size, enlarges if necessary
     *
     * @param size the actually used array lengths
     * @return itself (fluent design)
     */
    public DoubleIntDataSet resize(final int size) {
        lock().writeLockGuard(() -> {
            xValues.size(size);
            yValues.size(size);
        });
        return fireInvalidated(new UpdatedDataEvent(this, "increaseCapacity()"));
    }

    /**
     * clear old data and overwrite with data from 'other' data set
     *
     * @param other the source data set
     * @param copy true: perform a deep copy (default), false: reuse the other dataset's internal data structures (if applicable)
     * @return itself (fluent design)
     */


    /**
     * <p>
     * Initialises the data set with specified data.
     * </p>
     * Note: The method copies values from specified double arrays.
     *
     * @param xValues X coordinates
     * @param yValues Y coordinates
     * @return itself
     */
    public DoubleIntDataSet set(final double[] xValues, final int[] yValues) {
        return set(xValues, yValues, true);
    }

    /**
     * <p>
     * Initialises the data set with specified data.
     * </p>
     * Note: The method copies values from specified double arrays.
     *
     * @param xValues X coordinates
     * @param yValues Y coordinates
     * @param copy true: makes an internal copy, false: use the pointer as is (saves memory allocation
     * @return itself
     */
    public DoubleIntDataSet set(final double[] xValues, final int[] yValues, final boolean copy) {
        return set(xValues, yValues, -1, copy);
    }

    /**
     * <p>
     * Initialises the data set with specified data.
     * </p>
     * Note: The method copies values from specified double arrays.
     *
     * @param xValues X coordinates
     * @param yValues Y coordinates
     * @param nSamples number of samples to be copied
     * @param copy true: makes an internal copy, false: use the pointer as is (saves memory allocation
     * @return itself
     */
    public DoubleIntDataSet set(final double[] xValues, final int[] yValues, final int nSamples, final boolean copy) {
        AssertUtils.notNull(X_COORDINATES, xValues);
        AssertUtils.notNull(Y_COORDINATES, yValues);
        final int dataMaxIndex = Math.min(xValues.length, yValues.length);
        if (nSamples >= 0) {
            AssertUtils.indexInBounds(nSamples, xValues.length + 1, "xValues bounds");
            AssertUtils.indexInBounds(nSamples, yValues.length + 1, "yValues bounds");
        }
        final int nSamplesToAdd = nSamples >= 0 ? Math.min(nSamples, xValues.length) : xValues.length;

        lock().writeLockGuard(() -> {
            getDataLabelMap().clear();
            getDataStyleMap().clear();
            if (copy) {
                if (this.xValues == null) {
                    this.xValues = new DoubleArrayList();
                }
                if (this.yValues == null) {
                    this.yValues = new IntArrayList();
                }
                resize(0);

                this.xValues.addElements(0, xValues, 0, nSamplesToAdd);
                this.yValues.addElements(0, yValues, 0, nSamplesToAdd);
            } else {
                this.xValues = DoubleArrayList.wrap(xValues, nSamplesToAdd);
                this.yValues = IntArrayList.wrap(yValues, nSamplesToAdd);
            }

            // invalidate ranges
            getAxisDescriptions().forEach(AxisDescription::clear);
        });
        return fireInvalidated(new UpdatedDataEvent(this));
    }

    /**
     * replaces point coordinate of existing data point
     *
     * @param index data point index at which the new data point should be added
     * @param newValue new data point coordinate
     * @return itself (fluent design)
     */
    @Override
    public DoubleIntDataSet set(final int index, final double... newValue) {
        return set(index, newValue[0], newValue[1]);
    }

    /**
     * replaces point coordinate of existing data point
     *
     * @param index the index of the data point
     * @param x new horizontal coordinate
     * @param y new vertical coordinate N.B. errors are implicitly assumed to be zero
     * @return itself (fluent design)
     */
    public DoubleIntDataSet set(final int index, final double x, final int y) {
        lock().writeLockGuard(() -> {
            final int dataCount = Math.max(index + 1, this.getDataCount());
            xValues.size(dataCount);
            yValues.size(dataCount);
            xValues.elements()[index] = x;
            yValues.elements()[index] = y;
            getDataLabelMap().remove(index);
            getDataStyleMap().remove(index);

            // invalidate ranges
            getAxisDescriptions().forEach(AxisDescription::clear);


        });
        return fireInvalidated(new UpdatedDataEvent(this, "set - single"));
    }

    public DoubleIntDataSet set(final int index, final double[] x, final int[] y) {
        lock().writeLockGuard(() -> {
            resize(Math.max(index + x.length, xValues.size()));

            /*        for (int i = 0; i < x.length; i++)
                    {
                        xValues.elements()[index + i] = x[i];
                        yValues.elements()[index + i] = y[i];
                    }*/

            System.arraycopy(x, 0, xValues.elements(), index, x.length);
            System.arraycopy(y, 0, yValues.elements(), index, y.length);
            getDataLabelMap().remove(index, index + x.length);
            getDataStyleMap().remove(index, index + x.length);

            // invalidate ranges
            getAxisDescriptions().forEach(AxisDescription::clear);
        });
        return fireInvalidated(new UpdatedDataEvent(this, "set - via arrays"));
    }

    /**
     * Trims the arrays list so that the capacity is equal to the size.
     *
     * @see java.util.ArrayList#trimToSize()
     * @return itself (fluent design)
     */
    public DoubleIntDataSet trim() {
        lock().writeLockGuard(() -> {
            xValues.trim(0);
            yValues.trim(0);
        });
        return fireInvalidated(new UpdatedDataEvent(this, "increaseCapacity()"));
    }
}