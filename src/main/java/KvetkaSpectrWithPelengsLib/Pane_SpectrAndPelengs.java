package KvetkaSpectrWithPelengsLib;


import KvetkaSpectrWithPelengsLib.Drawing.DrawArea;
import KvetkaSpectrWithPelengsLib.Interface.ICallBackCommonChartEvents;
import KvetkaSpectrWithPelengsLib.Interface.ICallBackSignalEvents;
import KvetkaSpectrWithPelengsLib.Signal.SignalButton;
import KvetkaSpectrWithPelengsLib.Signal.SignalMath;
import KvetkaSpectrWithPelengsLib.Signal.SignalParameters;
import de.gsi.dataset.utils.ByteArrayCache;
import de.gsi.dataset.utils.DoubleArrayCache;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;


public class Pane_SpectrAndPelengs extends AnchorPane implements Initializable {
    @FXML
    private Button _1_1btn;

    @FXML
    private Button moveLeftbtn;

    @FXML
    private Button moveRightbtn;

    @FXML
    private Button movePlusbtn;

    @FXML
    private Button moveZoombtn;

    @FXML
    private OnlyChart_Spectr onlyChart;

    @FXML
    private PointPelengs pelengs;

    @FXML
    private Label thresholdLabel;

    @FXML
    private Label frequencyLabel;

    @FXML
    private Label levelLabel;

    @FXML
    private HBox HBoxWithSignalButtons;

    private final int leftBorderXAxis = 1500;
    private final int rightBorderXAxis = 30000;

    private double porog = -80;
    private double DeltaFreq = 2;
    private int btnId = -1; //flag
    private ICallBackSignalEvents iCallBackSignalEvents;
    private ICallBackCommonChartEvents iCallBackCommonChartEvents;


    /**
     * Creates a new instance of <code>Pane_SpectrAndPelengs</code>
     * @throws IOException
     */
    public Pane_SpectrAndPelengs() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Spectr_1.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        onlyChart.getPlotArea().setOnMouseClicked(this::handleClickMouseEvent);
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        onlyChart.setZoomRange(leftBorderXAxis, rightBorderXAxis);
        onlyChart.setZoomDepth(10);

        onlyChart.getXAxis().minProperty().bindBidirectional(pelengs.getXAxis().minProperty());
        onlyChart.getXAxis().maxProperty().bindBidirectional(pelengs.getXAxis().maxProperty());


        levelLabel.setText("U = "
                .concat(String.valueOf(Math.round(onlyChart.getValueY_Cross() * 10)/10.0))
                .concat(" дБ   "));
        thresholdLabel.setText("h (порог) = "
                .concat(String.valueOf(Math.round(onlyChart.getValue_Porog()* 10)/10.0))
                .concat(" дБ   "));
        frequencyLabel.setText("F = "
                .concat(String.valueOf((Math.round(onlyChart.getValueX_Cross()*1000)/1000.0)))
                .concat(" кГц   "));

        onlyChart.getValueProperty_Porog().addListener((observableValue, number, t1) -> {
            porog = (double) Math.round(onlyChart.getValue_Porog() * 10) / 10.0;
            thresholdLabel.setText("h (порог) = "
                    .concat(String.valueOf(porog))
                    .concat(" дБ   "));
            iCallBackCommonChartEvents.thresholdChanged(porog);
        });
        onlyChart.getValuePropertyX_Cross().addListener((observableValue, number, t1) -> frequencyLabel.setText("F = " +  Math.round(onlyChart.getValueX_Cross() * 1000)/1000.0 + " кГц   "));

        onlyChart.getValuePropertyY_Cross().addListener((observableValue, number, t1) -> {
            pelengs.setValueXIndicatorCustom(onlyChart.getValueX_Cross());
            levelLabel.setText("U = " + (Math.round(onlyChart.getValueY_Cross()*10)/10.0)  + " дБ   ");
        });
    }

    /**
     * displays a new dataset (start frequency equals 1500, end frequency equals 30000)
     *
     * @param newDataSet array of level values
     */
    public void UpdateSpectr(double[] newDataSet)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;
        onlyChart.PlotSpectr(newDataSet);
        UpdateSignals();

        ClearChaches();
    }

    /**
     * displays a new dataset (start frequency equals 1500)
     *
     * @param newDataSet array of level values
     * @param endFreq end frequency of the displayed spectrum (double)
     */
    public void UpdateSpectr(double[] newDataSet, double endFreq)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;
        onlyChart.PlotSpectr(endFreq, newDataSet);
        UpdateSignals();

        ClearChaches();
    }

    /**
     * displays a new dataset
     *
     * @param newDataSet array of level values
     * @param startFreq start frequency of the displayed spectrum
     * @param endFreq end frequency of the displayed spectrum (double)
     */
    public void UpdateSpectr(double[] newDataSet, double startFreq, double endFreq)
    {
        if(newDataSet == null || newDataSet.length == 0)
            return;
        onlyChart.PlotSpectr(startFreq, endFreq, newDataSet);
        UpdateSignals();

        ClearChaches();
    }


    /**
     * displays a new datasets (Pelengs)
     *
     * @param angle array of peleng angles values (double)
     * @param freq array of peleng frequency values (double)
     */
    public void UpdatePelengs(double[] angle, double[] freq) {
        if(angle == null || freq == null || angle.length != freq.length || angle.length == 0)
            return;
        pelengs.PlotPelengs(angle, freq);
        onlyChart.PlotPelengs(freq);

        ClearChaches();
    }


    /**
     * displays a new datasets (Pelengs)
     *
     * @param angle array of peleng angles values (double)
     * @param stepFrequency step of frequency
     */
    public void UpdatePelengs(double[] angle, double stepFrequency) {
        if(angle == null)
            return;
        double[] freq = pelengs.PlotPelengs(angle, stepFrequency, onlyChart.getMinimumXValue());
        onlyChart.PlotPelengs(freq);

        ClearChaches();
    }

    /**
     * displays a new datasets (Pelengs)
     *
     * @param angle array of peleng angles values (double)
     * @param stepFrequency bearing (frequency) measurement step
     * @param startFrequency start frequency for angles in array <code>angle</code>
     */
    public void UpdatePelengs(double[] angle, double stepFrequency, double startFrequency)  {
        if(angle == null )
            return;
        double[] freq = pelengs.PlotPelengs(angle, stepFrequency, startFrequency);
        onlyChart.PlotPelengs(freq);

        ClearChaches();
    }

    private void UpdateSignals()
    {
        if(btnId != -1)
        {
            int[] y = SignalMath.ConvertDoubleMassiveToInt(onlyChart.getYList());

            isSignalsExist(y);
        }
    }


    /**
     * change min pixel distance for reduction algorithm
     *
     * @param minPixelDistance The larger the value, the faster the output works, the smaller the more correct the values will be displayed
     */
    public void setMinPixelDistanceForReductionAlgorithm(int minPixelDistance)
    {
        onlyChart.setMinPixelDistanceForReductionAlgorithm(minPixelDistance);
    }

    /**
     * set a number that means which elements from the data set will be selected for display
     *
     * @param value every <code>count<code/> element selected from data set for display on chart
     */
    public void setSelectEveryValueCount(int value)
    {
        onlyChart.setSelectEvery_Count(value);
    }


    /**
     * set a number of old pelengs which will display on the chart
     *
     * @param amountDisplayedOldPelengs a number of old pelengs which will display on the chart
     */
    public void setAmountDisplayedOldPelengs(int amountDisplayedOldPelengs)
    {
        pelengs.setAmountDisplayedOldPelengs(amountDisplayedOldPelengs);
    }

    public DrawArea getDrawingArea() {return onlyChart.getDrawArea();}

    public void _1_1Button(javafx.event.ActionEvent actionEvent) {
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(onlyChart.getMinimumXValue(), onlyChart.getMaximumXValue());
        onlyChart.getXAxis().forceRedraw();
        pelengs.getXAxis().setAutoRanging(false);
        pelengs.getXAxis().set(onlyChart.getMinimumXValue(), onlyChart.getMaximumXValue());
        pelengs.getXAxis().forceRedraw();
    }

    public void MoveLeftButton(ActionEvent actionEvent) {
        double diff = (onlyChart.getXAxis().getMax() - onlyChart.getXAxis().getMin())/4;
        double X1 = onlyChart.getXAxis().getMin() - diff;
        if(X1 < leftBorderXAxis) {
            X1 = leftBorderXAxis;
            diff = 0;
        }
        final double X2 = onlyChart.getXAxis().getMax() - diff;
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(X1, X2);
        onlyChart.getXAxis().forceRedraw();
        pelengs.getXAxis().setAutoRanging(false);
        pelengs.getXAxis().set(X1, X2);
        pelengs.getXAxis().forceRedraw();
    }

    public void MoveRightButton(ActionEvent actionEvent) {
        double diff = (onlyChart.getXAxis().getMax() - onlyChart.getXAxis().getMin())/4;
        double X2 = onlyChart.getXAxis().getMax() + diff;
        if(X2 > rightBorderXAxis) {
            X2 = rightBorderXAxis;
            diff = 0;
        }
        final double X1 = onlyChart.getXAxis().getMin() + diff;
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(X1,X2);
        onlyChart.getXAxis().forceRedraw();
        pelengs.getXAxis().setAutoRanging(false);
        pelengs.getXAxis().set(X1,X2);
        pelengs.getXAxis().forceRedraw();
    }

    public void PlusZoomButton(ActionEvent actionEvent) {
        final double half = (onlyChart.getXAxis().getMax()+ onlyChart.getXAxis().getMin())/2;
        final double diffmin = (half - onlyChart.getXAxis().getMin())*0.9;
        if(diffmin <= 0)
        { onlyChart.getXAxis().setAutoRanging(false);
            return;
        }
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(half-diffmin,half+diffmin);
        onlyChart.getXAxis().forceRedraw();
        pelengs.getXAxis().setAutoRanging(false);
        pelengs.getXAxis().set(half-diffmin,half+diffmin);
        pelengs.getXAxis().forceRedraw();
    }

    public void MinusZoomButton(ActionEvent actionEvent) {
        final double half = (onlyChart.getXAxis().getMax()+ onlyChart.getXAxis().getMin())/2;
        final double diffmin = (half - onlyChart.getXAxis().getMin())*(1/0.9);
        if(diffmin <= 0 || diffmin >= half)
        {
            onlyChart.getXAxis().setAutoRanging(false);
            return;
        }
        onlyChart.getXAxis().setAutoRanging(false);
        onlyChart.getXAxis().set(half-diffmin,half+diffmin);
        onlyChart.getXAxis().forceRedraw();
        pelengs.getXAxis().setAutoRanging(false);
        pelengs.getXAxis().set(half-diffmin,half+diffmin);
        pelengs.getXAxis().forceRedraw();
    }
    /**
     * handler of the button ...
     */
    public void ExtendRangeButton(ActionEvent actionEvent) {
        double value = onlyChart.getYAxis().getMax();
        if(value < 120)
            value += 10;
        onlyChart.setMaximumYValue(value);
    }

    /**
     * handler of the button ...
     */
    public void NarrowRangeButton(ActionEvent actionEvent) {
        double value = onlyChart.getYAxis().getMax();
        if(value > 0)
            value -= 10;
        onlyChart.setMaximumYValue(value);
    }


    /**
     * registers a callback to access events
     *
     * @param iCallBackSignalEvents instance of class which implements <code>ICallBackSignalEvents_1<code/> interface
     */
    public void registerCallBackSignalEvents(ICallBackSignalEvents iCallBackSignalEvents)
    {
        this.iCallBackSignalEvents = iCallBackSignalEvents;
    }

    /**
     * registers a callback to access events
     *
     * @param iCallBackCommonChartEvents instance of class which implements <code>ICallBackCommonChartEvents<code/> interface
     */
    public void registerCallBackCommonChartEvents(ICallBackCommonChartEvents iCallBackCommonChartEvents)
    {
        this.iCallBackCommonChartEvents = iCallBackCommonChartEvents;
    }

    /**
     * get button for signal
     *
     *
     * @param id id of signal
     * @return button for signal
     */
    public SignalButton GetButtonObject(int id)
    {
        return ((SignalButton)HBoxWithSignalButtons.getChildren().get(id));
    }

    /**
     * get button for signal (the frequency must be set in the range + - 3 kHz for the signal to be found)
     *
     *
     * @param freq freq of signal
     * @return button for signal
     */
    public SignalButton GetButtonObject(double freq)
    {
        for(int  i = 0; i < HBoxWithSignalButtons.getChildren().size(); i++) {
            double realSignalFrequency = ((SignalButton) HBoxWithSignalButtons.getChildren().get(i)).getSignalParameters().getFrequency();
            if (realSignalFrequency >= (freq - 3) && realSignalFrequency <= freq + 3)
                return ((SignalButton) HBoxWithSignalButtons.getChildren().get(i));
        }
        return null;
    }
    /**
     * handler of the button designed for delete selected signal
     */
    public void OnDeleteSignals(ActionEvent actionEvent)
    {
        for(int i = 0; i < HBoxWithSignalButtons.getChildren().size(); i++)
        {
            SignalButton signalButton = (SignalButton)HBoxWithSignalButtons.getChildren().get(i);
            SignalParameters signalParameters = signalButton.getSignalParameters();


            if(signalButton.getIsCheckedStatus())
            {
                iCallBackSignalEvents.signalDeleted(signalParameters.getFrequency(), i, signalButton.getAnalogReconFWSModel());
                onlyChart.deleteRangeIndicator(signalParameters.getCenterFrequency());
                HBoxWithSignalButtons.getChildren().remove(i);
                System.gc();
                i--;
            }
            else
            {
                signalButton.setSignalId(i);
            }
            btnId = i;
            if(HBoxWithSignalButtons.getChildren().size() == 0)
            {
                HBoxWithSignalButtons.getChildren().clear();
                btnId = -1;

            }
            else {
                if(i != -1)
                    onlyChart.changeColorXRangeIndicator(signalParameters.getCenterFrequency());
            }

        }
    }

    /**
     * create new button for signal
     *
     *
     * @param signalParameters object which contains information about the signal
     * @return button for signal
     */
    private SignalButton CreateButton(SignalParameters signalParameters) throws IOException {
        SignalButton btn = new SignalButton();
        btn.setSignalParameters(signalParameters);
        btn.setOnMouseClicked(event -> {
            int id = HBoxWithSignalButtons.getChildren().indexOf(event.getSource());
            btnId = id;
            SignalParameters signal_Parameters = ((SignalButton)HBoxWithSignalButtons.getChildren().get(btnId)).getSignalParameters();
        });
        return btn;
    }


    /**
     * checks for the presence of a signal set for tracking
     *
     * @param dataset array of level values
     */
    private void isSignalsExist(int[] dataset)
    {
        for(int j = 0; j < HBoxWithSignalButtons.getChildren().size(); j++)
        {
            SignalButton signalButton = (SignalButton)HBoxWithSignalButtons.getChildren().get(j);
            SignalParameters signalParameters = signalButton.getSignalParameters();

            final int startIndex = signalParameters.getIndexAtWhichSelectedAreaStarts();
            final int endIndex = signalParameters.getIndexAtWhichSelectedAreaEnds();
            int[] subSpectrum = Arrays.copyOfRange(dataset, startIndex, endIndex);
            signalParameters.setDataSet(subSpectrum);


            boolean deleteFlag = isSignalExist(signalParameters.getIndexAtWhichSelectedAreaStarts(), signalParameters.getIndexAtWhichSelectedAreaEnds(), dataset);

            if(!deleteFlag && signalParameters.isSignalExist())
            {
                signalButton.setIndicatorOfExistSignal(false);
                iCallBackSignalEvents.signalBecameUnavailable(signalButton.getSignalParameters().getFrequency(), j, signalButton.getAnalogReconFWSModel());
            }
            else if (deleteFlag && !signalParameters.isSignalExist())
            {
                signalButton.setIndicatorOfExistSignal(true);
                iCallBackSignalEvents.signalBecameAvailableAgain(signalParameters.getFrequency(), j, signalButton.getAnalogReconFWSModel());
                signalButton.setFreq(signalParameters.getFrequency());
            }
        }
    }

    private boolean isSignalExist(int indexAtWhichSelectedAreaStarts, int indexAtWhichSelectedAreaEnds, int[] dataset)
    {
        boolean deleteFlag = false;
        for(int i = indexAtWhichSelectedAreaStarts + 1; i < indexAtWhichSelectedAreaEnds; i++)
        {
            if(dataset[i - 1] < porog && dataset[i]>=porog )
            {
                while (i < indexAtWhichSelectedAreaEnds - 1)
                {
                    if(dataset[i] >= porog)
                    {
                        deleteFlag = true;
                        break;
                    }
                    i++;
                }
                break;
            }
        }
        return deleteFlag;
    }

    /**
     * handle double click on chart
     */
    private void handleClickMouseEvent(MouseEvent mouseEvent) {
        //handle double click event.
        if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
            mouseEvent.consume();
            // function for filter signals and not signals

            // getting massive of frequency
            double[] x = onlyChart.getXList();
            // getting massive of level
            double[] y = onlyChart.getYList();

            if(x.length == 0 || y.length == 0)
                return;

            // cutting zero part of massive
            int k = 2;
            while (x[k] !=0)
            {
                k++;
                if(k == x.length)
                    break;
            }

            y = Arrays.copyOfRange(y,0,k);
            x = Arrays.copyOfRange(x,0,k);

            // getting mouse coordinate
            Point2D mouseClickCord = onlyChart.getValuePosFromMouseClick(mouseEvent.getSceneX(), mouseEvent.getSceneY());
            double clickXCoord = mouseClickCord.getX();
            // getting index of mouse coordinate in frequency array


            double startFreqOfSelectedArea  = clickXCoord - DeltaFreq;
            double endFreqOfSelectedArea = clickXCoord + DeltaFreq;

            final int startIndexOfSelectedArea = onlyChart.getAllDatasets().get(0).getIndex(0,startFreqOfSelectedArea);
            final int endIndexOfSelectedArea = onlyChart.getAllDatasets().get(0).getIndex(0,endFreqOfSelectedArea);

            startFreqOfSelectedArea = Math.round(x[startIndexOfSelectedArea] * 100000.)/100000.;
            endFreqOfSelectedArea = Math.round(x[endIndexOfSelectedArea] * 100000.)/100000.;

            if(isSignalConsistByBounds(startFreqOfSelectedArea, endFreqOfSelectedArea, DeltaFreq) == -1) {//check if a signal has met such parameters before

                onlyChart.addXRangeIndicator(startFreqOfSelectedArea, endFreqOfSelectedArea);

                try {

                    boolean isSignalExist = isSignalExist(startIndexOfSelectedArea, endIndexOfSelectedArea,  SignalMath.ConvertDoubleMassiveToInt(y));
                    SignalParameters signalParameters = new SignalParameters(Arrays.copyOfRange(y, startIndexOfSelectedArea, endIndexOfSelectedArea), startIndexOfSelectedArea, endIndexOfSelectedArea, startFreqOfSelectedArea, endFreqOfSelectedArea, isSignalExist);

                    HBoxWithSignalButtons.getChildren().add(CreateButton(signalParameters));
                    btnId = HBoxWithSignalButtons.getChildren().size() - 1;
                    ((SignalButton) HBoxWithSignalButtons.getChildren().get(HBoxWithSignalButtons.getChildren().size() - 1)).setSignalId(HBoxWithSignalButtons.getChildren().size() - 1);

                    iCallBackSignalEvents.signalAdded(signalParameters.getFrequency(), HBoxWithSignalButtons.getChildren().size() - 1, signalParameters.getAnalogReconFWSModel());
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }
    }

    private int isSignalConsistByBounds(double leftBound, double rightBound, double delta)
    {
        for(int i = 0; i < HBoxWithSignalButtons.getChildren().size(); i++)
        {
            SignalParameters signalParameters = ((SignalButton)HBoxWithSignalButtons.getChildren().get(btnId)).getSignalParameters();

            final double somethingLeftBound = signalParameters.getFrequencyAtWhichSelectedAreaStarts();
            final double somethingRightBound = signalParameters.getFrequencyAtWhichSelectedAreaEnds();
            if(somethingLeftBound <= leftBound + delta && somethingLeftBound >= leftBound - delta && somethingRightBound <= rightBound + delta && somethingRightBound >= rightBound - delta)
                return i;
        }
        return -1;
    }


    private void ClearChaches()
    {
        DoubleArrayCache.getInstance().clear();
        ByteArrayCache.getInstance().clear();
    }
}
